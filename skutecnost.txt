tento soubor bude obsahovat rozdil mezi planem projektu a skutecnosti

Task name | Deadline | Real deadline | Reason no.
-------------------------------------------------
Profiling | 5.4.     | 13.4.         | 1
Tests     | 13.3.    | 23.3.         | 2
Package   | 13.4.    | 20.4.         | 3

Reasons:
Some delays were also caused by global pandemic outbreak.

1)  Missing experience with the various profiler tools took suprisingly 
    huge chunk of time - time was invested into researching and trying tools
    and evaluating their benefits. Some of the tools were eliminated right 
    from the start, however some seemed very promising. Some time (hours) were
    invested into automated `perf` data collection, however all statistical
    profilers that do not use code instumentation seem to be useless with the 
    size of current dataset. For datasets of bigger size, the results are very
    promising. The final decision was to use callgrind (because the instrumen-
    tation).

    Lessons learned:
        - before designing project schedule (profiling), make a quick analysis
        about which tools are available, and why they wouldn't work
        - small datasets => code instrumentation would be probably involved

    Summary:
        - deadline would be realistic, have I had previous experience with
        evaluation of the problem (profiling itself is rather straightforward)

2)  The delays in the interface design and problems with getting the gtest
    library working (mainly linking problems). Also, we didn't decide soon 
    enough on the additional function, we were supposed to chose, therefore it
    couldn't have been subject of testing and tests got delayed even more.

3)  Insufficient knowledge in the process of creating packages and not fully
    working build process. However there was a semi-working version of package
    on deadline, but it had many flaws, therefore couldn't be considered final.
    


