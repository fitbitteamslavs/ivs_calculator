#include <iostream>
#include <libcalc.h>

using namespace std;

double setCommandFunction(double oldCalculatorValue, double newValue) {
    (void) oldCalculatorValue;
    return newValue;
}

double noOperationCommandFunction(double oldCalculatorValue, double newValue) {
    (void) newValue;
    return oldCalculatorValue;
}


Command::Command(CommandFunction cmdFunction, double value):
    cmdFunction(cmdFunction), 
    value(value){}

double Command::apply(double calculatorState) {
	return this->cmdFunction(calculatorState, this->value);
}

Calculator::Calculator(double initialState): 
    currentValue(initialState){
        history.push_back(currentValue);
        changeIndex = 0;
    }

double Calculator::applyCommand(Command c){
    // Store history
    while (changeIndex < (int) (history.size() - 1)) {
        history.pop_back(); // We are somewhere in history - destroy everything forwards
    }
    this->changeIndex++;

	this->currentValue = c.apply(this->currentValue);
    this->history.push_back(currentValue);
	return this->currentValue;
}

void Calculator::printCurrentState() {
	cout << "Calculator state: " << this->currentValue << std::endl;
}

double Calculator::historyBackward() {
    if (this->changeIndex == 0) {
        throw std::runtime_error("Cannot go backward in history, already at the beginning.");
    }
    // (c, v) ----> (c, v) ----> (c, v) ---> actual_value
    //                              ^- changeIndex
    //
    // AFTER
    // (c, v) ----> (c, v) ----> (c, v) ---> actual_value
    //                 ^- changeIndex
    //
    this->changeIndex--;      // Move backwards in history
    currentValue = history.at(changeIndex);
    return currentValue;
}

double Calculator::historyForward() {
    // (v) ----> (v) ----> (actual_value)
    // //         ^- changeIndex
    // AFTER
    // (v) ----> (v) ----> (actual_value)
    //                              ^- changeIndex
    
    if ((unsigned long) this->changeIndex + 1 == history.size()) {
        throw std::runtime_error("Cannot go forward in history, already at the lastest change.");
    }
    this->changeIndex++;
    this->currentValue = history.at(changeIndex);
    return currentValue;
}

double Calculator::getCurrentState(){
	return this->currentValue;
}


CommandFactory::CommandFactory(Calculator *calculator):
    calculator(calculator){}

void CommandFactory::checkNthPowerArguments(double base, double exponent) {
    if (base < 0) {
        if (!libcalcMath::isInteger(exponent)) {
            throw std::invalid_argument("Cannot raise negative base to non integer power.");	
        }
    }
    if (base == 0) {
        if (exponent < 0){
            throw std::invalid_argument("Cannot raise 0 base to negative power");
        }
    }
}

void CommandFactory::checkFactorialArguments(double factorialBase) {
    // Cannot take factorial from negative number, or _non_ integer value 
    if (factorialBase < 0 || ! libcalcMath::isInteger(factorialBase)) {
        throw std::invalid_argument("Cannot take factorial from rational or negative number.");
    }
}

void CommandFactory::checkNthRootArguments(double base, double nthPower) {
    (void) base; // Ignore base argument
    // cannot take the 0nth root
    if (libcalcMath::doubleEqual(nthPower, 0)) {
        throw std::invalid_argument("Cannot take 0th root.");
    }
    if (calculator->getCurrentState() < 0) {
        throw std::invalid_argument("Cannot take root of negative number.");
    }
}
void CommandFactory::checkLogArgumentsValid(double value, double base) {
    if (value <= 0) {
        throw std::invalid_argument("Cannot take log. of nonpositive value.");
    }
    if (base <= 0) {
        throw std::invalid_argument("Cannot take log. with nonpositive base.");
    }

    if (base == 1.0) {
        throw std::invalid_argument("Logarithm base cannot be 1.");
    }
}

bool CommandFactory::areCommandArgumentsValid(Command::CommandType cmdType, double operand1, double operand2) {
    // operand1 is calculator state
    // operand2 is the second operation argument
    switch (cmdType) {
        case Command::CommandType::Div:
            if (libcalcMath::doubleEqual(operand2, 0)) {
                throw std::invalid_argument("Cannot divide by zero.");
            }
            return true;
        case Command::CommandType::NthRoot:
            this->checkNthRootArguments(operand1, operand2);
            return true;
        case Command::CommandType::NthPower:
            this->checkNthPowerArguments(operand1, operand2);
            return true;
        case Command::CommandType::Factorial:
            this->checkFactorialArguments(operand1);
            return true;
        case Command::CommandType::Log:
            this->checkLogArgumentsValid(operand1, operand2);
            return true;
        default:
            return true;
    }
}

Command CommandFactory::createCommand(Command::CommandType cmdType, double operand2) {
    // throws when arugments are invalid
    this->areCommandArgumentsValid(cmdType, calculator->getCurrentState(), operand2);

    // Handle those commands separatively - they are not math functions
	if (cmdType == Command::CommandType::SetCalculatorValue) {
		return Command(setCommandFunction, operand2);
	} else if (cmdType == Command::CommandType::NoOperation) {
		return Command(noOperationCommandFunction, operand2);
	}

    // CommandType is used as lookup field to static array of math functions
	return Command(libcalcMath::mathFunctions[cmdType], operand2);
}
