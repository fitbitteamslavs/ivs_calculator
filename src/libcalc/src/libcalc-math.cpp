#include <cmath>
#include <libcalc-math.h>

namespace libcalcMath {

	bool doubleEqual(double a, double b) {
		return std::abs(a - b) < libcalcMath::EPS;
	}

    bool isInteger(double num){
        return std::floor(num) == num;
    }

	double add(double a, double b) {
		return a + b;
	}

	double sub(double a, double b) {
		return a - b;
	}
	double mul(double a, double b) {
		return a*b;
	}

	double div(double a, double b) {
		return a/b;
	}

	double factorial(double value, double ignored) {
		if (value == 0.0) {
			return 1;
		}
		(void) ignored;
		double result = 1.0;
		for (double i = value; i > 0; i-=1.0){
			result *= i;
            if (std::isinf(result)) {
                break;
            }
		}
		return result;
	}

	double power(double base, double exponent) {
		return std::pow(base, exponent);
	}

	double root(double number, double root) {
		return std::pow(number, 1/root);
	}

    double sqrt(double base, double ignored) {
        (void) ignored;
        return root(base, 2);
    }

    double log(double value, double base) {
        return std::log(value) / std::log(base);
    }
}
