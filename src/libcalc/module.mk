MODULES += libcalc
SOURCES += $(wildcard libcalc/src/*.cpp)
HEADERS += $(wildcard libcalc/include/*.h)
INCLUDES += -Ilibcalc/include/

LD_LIBRARY_FLAGS := -shared
