#ifndef _LIBCALC_MATH_H
#define _LIBCALC_MATH_H
#include <functional>
#include <iostream>
#include <cmath>

/**
 * @brief Header for libcalc-math - library module that provides various
 *        mathematical functions used in conputator (IVS calculator project).
 * @file src/libcalc/include/libcalc-math.h
 * @author IPDKteam
 * @date 23.4.2020
 */

/**
 * @ingroup Library
 * @{
 */

/**
 * Mathematical core of the library libcalc. Provides implementations
 * of the mathematical functions used by the GUI.
 */
namespace libcalcMath {
    const double EPS = 0.0005;

    /**
     * Checks whether the two provided numbers can be considered equal.
     * Uses intrnal constant libcalcMath::EPS to determine what is equal 
     * and what is not.
     *
     * @returns true when @p a is considered to be equal to @b
     */
	bool doubleEqual(double a, double b);

    /**
     * Checks whether @p num is an integer represented as double. In other
     * words checks, whether the fractional part of double is 0.
     *
     * @param num Number to be evaluated as integer or not
     * @returns true when the number has no fractional part, false otherwise
     */
    bool isInteger(double num);

    /**
     * Calculates the addition of a+b
     *
     * @param a the summand
     * @param b the summand
     * @returns The sum of provided arguments.
     */
	double add(double a, double b);

    /**
     * Calculates the subtraction a-b
     *
     * @param a the minuend
     * @param b the subtrahend
     * @returns The result of subtraction a-b
     */
	double sub(double a, double b);

    /**
     * Calculates the multiplication of a*b
     *
     * @param a the multiplier
     * @param b the multiplicant
     * @returns The result of multiplication a*b
     */
	double mul(double a, double b); 

    /**
     * Calculates the division of a/b.
     *
     * @param a the divident
     * @param b the divisor
     * @warning None of the arguments are checked for validity.
     * @returns The result of division a/b
     */
	double div(double a, double b); 

    /**
     * Calculates the factorial from @p value.
     *
     * @param value value from which the factorial should be taken
     *              Value should be integer, otherwise undesired results will
     *              be returned.
     * @param ignored ignored parameter
     * @note It is adviced to realize the limits of double datatype, and that
     *       the result may be inf.
     * @returns Factorial of value (value!)
     */
	double factorial(double value, double ignored);

    /**
     * Calculates the the nth power (specified by @p exponent) of the provided
     * base.
     * Equivalent experession is `result` = (base)^(power) 
     *
     * @param base Base (argument) for the power function.
     * @param root Specifies the order of power taken.
     * @warning None of the arguments are checked for validity.
     * @returns exponent-th power of @p base.
     */
	double power(double base, double exponent);

    /**
     * Calculates the nth (specified by @p root) root from given @p number.
     *
     * @param number Parameter to the root math function.
     * @param root Specifies the order of root taken
     * @warning None of the arguments are checked for validity.
     * @returns Nth root of `number`.
     */
	double root(double number, double root);

    /**
     * Calculates the square root from given number. Parameter `root` is ignored.
     *
     * @param number Parameter to the square root math function.
     * @param root Is ignored
     * @warning None of the arguments are checked for validity.
     * @returns Square root of `number`
     */
	double sqrt(double number, double root);

    /**
     * Calculates the logarithm from provided value. Provided arguments are 
     * used to in a manner that satisfies this equation: (base)^(result) = value,
     * where result is result of this function call.
     * The calculated expression is also equivalent to y = log(value)/log(base).
     *
     * @brief Calculates logarithm of value using `base` as logarithm base.
     * @param value Value from which the logarithm will be taken
     * @param bae Base to the logarith.
     * @warning None of the arguments are checked for validity.
     * @returns Logarithm satisfying abobe equations.
     */
	double log(double value, double base);

    /**
     * Static array of function pointers provided by this library. The order of provided
     * functions is such, that the CommandType enum numbers can be used directly as index
     * into this array for lookup.
     */
	static const std::function<double(double, double)> mathFunctions[] = {
			add,
			sub,
			mul,
			div,
			power,
			root,
            sqrt,
			factorial,
            log
	};
}

/** @} */
#endif
