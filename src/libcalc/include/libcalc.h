#ifndef __LIBCALC_H
#define __LIBCALC_H

#include <iostream>
#include <vector>
#include <functional>
#include <libcalc-math.h>

using namespace std;

/**
 * @brief Main header for libcalc - library that provides abstraction layer
 *        over mathematical functions used in conputator (IVS calculator project).
 * @file src/libcalc/include/libcalc.h
 * @author IPDKteam
 * @date 23.4.2020
 */

/**
 * @defgroup Library Mathematical library
 * @{
 */

typedef std::function<double(double, double)> CommandFunction;
/**
 * Internal function, which is the underlying function for CommandType::SetValue.
 * Function ignores the first provided value and returns the second - setting the
 * calculator value.
 *
 * @param   newValue            Value will be immediately returned.
 * @param   oldCalculatorValue  Value is ignored.
 * @returns                     The value of parameter oldCalculatorValue.
 */
double setCommandFunction(double oldCalculatorValue, double newValue);

/**
 * Internal function, which is the underlying function for CommandType::NoOperation.
 * Function always returns the first parameter, second is ignored (therefore acting
 * as a no operation function (calculator value is not mutated).
 *
 * @param   oldCalculatorValue  Value will be immediately returned.
 * @param   newValue            Value is ignored.
 * @returns                     The value of parameter oldCalculatorValue.
 */
double noOperationCommandFunction(double oldCalculatorValue, double newValue);

/**
 * Class that encapsulates an mathematical (or other) operation that calculator should execute.
 * All knowledge about the operation is hidden from Calculator, therefore Command is class is the
 * only class responsible for the mutation of calculator state.
 *
 * Normally initialization of this class should not be performed by hand, rather left this to
 * CommandFactory, which also checks whether the calculator state and the second operand would
 * not result in undefined state after the application of the mutation.
 *
 * For the list of operations supported see CommandFactory::createCommand function.
 */
class Command {
	public:
        /**
         * Constructor for a Command.
         *
         * @param cmdFunction   Lambda function that calculates the new value of calculator
         *                      when the command is applied.
         * @param value         Value which is the second operand to underlying cmdFunction,
         *                      the first one being calculator state.
         */
        Command(CommandFunction cmdFunction, double value);
        /**
         * Calculates the new calculator state from provided calculated state, and internally
         * stored value using cmdFunction.
         *
         * @note    Function does not mutate any internal fields, therefore has no sideeffects.
         *
         * @param calculatorState   State in which is the calculator (number)
         * @returns                 The new state for calculator.
         */
		double apply(double value);

        /**
         * Enum providing information (and means of creating) about which Commands (therefore operations)
         * are supported by the calculator.
         */
		enum CommandType {
			Add,
			Sub,
			Mul,
			Div,
			NthPower,
			NthRoot,
            Sqrt,
			Factorial,
            Log,
			NoOperation,
			SetCalculatorValue
		};
	private:
        /** Function which is used to calculate new calculator value from its current state and Command's internal value*/
		CommandFunction cmdFunction;
		double value;
};

/**
 * Class that encapsulates the state of calculator. Operator may communicate with calculator strictly by sending
 * commands which act on calculator state. Calculator knows nothing about operation being performed. Calculator
 * also stores whole history of calculated values and manages required invalidations should they be required.
 */
class Calculator {
	private:
        /** Vector holding the calculator history */
		vector<double> history;
        /** Current state of the calculator */
		double currentValue;
        /** Index to the history field, pointing to state in which we are (modified with historyForward/Backward) */
        int changeIndex; // where in history are we
	public:
        /**
         * Constructor for the Calculator class.
         *
         * @param initialState  Initial state in which the calculator is.
         */
		Calculator(double initialState);
        /**
         * Applies the provided Command (which encapsulates a specific math function)
         * causing the internal state to be modified accordingly. Also these changes 
         * are stored in history.
         *
         * @note    Should the calculator be currently in a value that is historical, and
         *          new command is applied, all history newer than actual value is invalidated.
         *
         * @param c     Command to be applied.
         *
         * @returns      The new state of the calculator.
         */
		double applyCommand(Command c);

        /**
         * Causes the internal state to be reverted one step to the past. 
         *
         * @throws  std::runtime_error When the calculator is in its oldest state.
         * @returns The state of the calculator into which was the calculator reverted.    
         */
		double historyBackward() noexcept(false);
        /**
         * Causes the internal state to be changed to newer value according to current
         * position in history. 
         *
         * @throws  std::runtime_error When the calculator is in its newest state.
         * @returns The state of the calculator into which was the calculator changed.    
         */
		double historyForward()  noexcept(false);

        /**
         * Helper function - prints the state of the calculator to the stdout.
         */
		void printCurrentState();

        /**
         * Returns the current state of the calculator.
         */
		double getCurrentState();
};

/**
 * Class responsible for creating (and initialization) of instances of the Command class.
 * It manages the creation of commands in such a way that it does not let the creation of commands happen,
 * when the act of mutation would result in undefined state given the current calculator state and the command
 * operand.
 */
class CommandFactory{
	public:
        /**
         * Creates a command of given CommandType. Before creating command, verification is done on the arguments, rising error
         * when the operation is not allowed mathematically. Returned command is an encapsulation of mathematical operation.
         * 
         * Table of supported CommandType-s: 
         *   - operand1 is the calculator state
         * | commandType              | operand1 interpreted as | operand2 interpreted as | Expression (new calculator state)
         * | --                       | --                      | --                      | --
         * | CommandType::Add         | summand                 | summand                 | operand1 + operand2
         * | CommandType::Sub         | minuend                 | subtrahend              | operand1 - operand2
         * | CommandType::Mul         | multiplier              | multiplicand            | operand1 * operand2
         * | CommandType::Div         | divident                | divisor                 | operand1 / operand2
         * | CommandType::NthRoot     | radicant                | digree                  | operand2-th root of operand1
         * | CommandType::Sqrt        | radicant                | *ignored* (constant 2)  | second root of operand1
         * | CommandType::NthPower    | base                    | exponent                | exponent-nth power base
         * | CommandType::Factorial   | factorial argument      | *ignored*               | operand1!
         * | CommandType::Log         | antilogarithm           | base                    | log with base=base and argument=antilogarithm
         * | CommandType::NoOperation | operand1                | *ignored*               | does not modify calculator state
         * | CommandType::SetValue    | *ignored*               | newValue                | newValue
         *
         * @param commandType The type of the command that will be created.
         * @param operand2  Second operand to the mathematical operation. See table above.
         *
         * @returns Command encapsulating given value.
         */
		Command createCommand(Command::CommandType commandType, double operand2);

        /**
         * Constructor for the command factory.
         *
         * @param calculator    Valid instance of calculator.
         */
		CommandFactory(Calculator *calculator);

	private:
        /** Lookup table for available math operations */
		static const CommandFunction* mathFunctions;
		Calculator *calculator;
        /** 
         * Verifies whether the provided arguments are valid for given command type.
         *
         * @param cmdType   Type of the command
         * @param operand1  First operand for the mathematical operation - will be 
         *                  internally interpreted according to the cmdType.
         * @param operand2  Second operand for the mathematical operation - will be 
         *                  internally interpreted according to the cmdType.
         * @throws std::invalid_argument    If the operands do not satisfy required criteria. The cause of the
         *                                  exception is contained within it.
         */
        bool areCommandArgumentsValid(Command::CommandType cmdType, double operand1, double operand2) noexcept(false);
        /**
         * Checks whether provided arguments are valid for the NthPower function.
         *
         * @throws std::invalid_argument    If the arguments do not suffice for operation.
         */
        void checkNthPowerArguments(double base, double exponent)   noexcept(false);
        /**
         * Checks whether provided argument is valid for factorial function.
         *
         * @throws std::invalid_argument    If the arguments do not suffice for operation.
         */
        void checkFactorialArguments(double factorialBase)          noexcept(false);
        /**
         * Checks whether provided arguments are valid for the NthRoot function.
         *
         * @throws std::invalid_argument    If the arguments do not suffice for operation.
         */
        void checkNthRootArguments(double base, double nthPower)    noexcept(false);
        /**
         * Checks whether provided arguments are valid for the Logarithm function.
         *
         * @throws std::invalid_argument    If the arguments do not suffice for operation.
         */
        void checkLogArgumentsValid(double value, double base)      noexcept(false);
};

/** @} */
#endif
