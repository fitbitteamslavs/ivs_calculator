#!/bin/bash
# This script is intended to be executed by the Makefile only.

if [ -z "$PACKAGE_DEST" ]; then
    echo "No package destination folder specified"
    exit 1
fi

if [ ! -d debian ]; then
    echo "There is no debian config directory"
    exit 1
fi

PACKAGE_GEN=/tmp/conputator_package

if [ -d "$PACKAGE_GEN" ]; then
    rm -rf "$PACKAGE_GEN"
fi

mkdir -p "$PACKAGE_GEN"

cp -r ../src "$PACKAGE_GEN"
cp ../gtest_config.mk "$PACKAGE_GEN"

cd "$PACKAGE_GEN"/src

QT_SELECT=qt5 dpkg-buildpackage -rfakeroot -uc -b

if [ $? != 0 ]; then
    echo "Something went wrong during the packaging"
    rm -rf "$PACKAGE_GEN"
    exit 1
fi

mkdir "$PACKAGE_DEST"
cp "$PACKAGE_GEN"/conputator_*.deb "$PACKAGE_DEST"

rm -rf "$PACKAGE_GEN"

