#!/bin/bash
# This script is intended to be executed by the Makefile only.

ZIP_PATH="$1"
if [[ ! "$ZIP_PATH" ]]; then
    echo "Script requires zip filename to be provided."
    exit 1
fi

DOC_PATH="$2"
if [[ ! "$DOC_PATH" ]]; then
    echo "Script requires documentation directory path."
    exit 1
fi

INSTALL_PATH="$3"
if [[ ! "$INSTALL_PATH" ]]; then
    echo "Script requires install directory path"
    exit 1
fi

PROJECT_ROOT="$4"
if [[ ! "$PROJECT_ROOT" ]]; then
    echo "Script requires project root"
    exit 1
fi

echo ' 
    ____             __                         ____   ____ 
   / __ \____ ______/ /_____  _____   _   __   / __ \ |___ \
  / /_/ / __ `/ ___/ //_/ _ \/ ___/  | | / /  / / / /  |_  /
 / ____/ /_/ / /__/ ,< /  __/ /      | |/ /  / /_/ /   __) | 
/_/    \__,_/\___/_/|_|\___/_/       |___/   \____(_)/____/ 
'

ZIP_FILENAME=$(basename "$ZIP_PATH")

if [ -d '/tmp/.ivs_pack' ]; then
    rm -rf '/tmp/.ivs_pack'
fi

mkdir -p /tmp/.ivs_pack/"$ZIP_FILENAME"/repo

echo "Packing up the project..."
echo "Copying source tree (with history) into /tmp/.ivs_pack/$ZIP_FILENAME/repo/"

cd "$PROJECT_ROOT"

find . \
    -not -path "./profiling/sprava.log" -and \
    -not -path "./profiling/sprava.aux" -and \
    -not -path "./profiling/sprava.fls" -and  \
    -not -path "./profiling/sprava.fdb_latexmk" -and  \
    -not -path "./src/debian/conputator/*" \
    -not -path "./src/debian/.*" \
    -not -path "./build*" -and  \
    -not -path "./doc*" -and  \
    -not -path "./googletest*" -and \
    -not -path "./*.d" -and \
    -not -path "./*compile_commands.json" -and \
    -not -path "./$ZIP_FILENAME".old.zip -and \
    -not -path "./$ZIP_FILENAME".zip -and \
    \( -type d -exec mkdir /tmp/.ivs_pack/"$ZIP_FILENAME"/repo/'{}' \; -or \
    -type f -exec cp '{}' /tmp/.ivs_pack/"$ZIP_FILENAME"/repo/'{}' \; \)

echo "Copying documentation from $DOC_PATH"
cp -r "$DOC_PATH" /tmp/.ivs_pack/"$ZIP_FILENAME"

echo "Copying package installer from $INSTALL_PATH"
cp -r "$INSTALL_PATH" /tmp/.ivs_pack/"$ZIP_FILENAME"

if [[ -f "$ZIP_PATH.zip" ]]; then
    echo "Detected old $ZIP_PATH.zip moving to '$ZIP_PATH.old.zip'"
    mv "$ZIP_PATH.zip" "$ZIP_PATH".old.zip
fi

cd /tmp/.ivs_pack
zip "$ZIP_PATH" -u -r .
