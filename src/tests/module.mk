MODULES += tests
SOURCES += $(wildcard tests/*.cpp)
INCLUDES +=-I$(GTEST_HOME)/googletest/include/

LD_TESTS_LIBS=-lgtest -lgtest_main -lcalc -lpthread
LD_TESTS_FLAGS=-L$(GTEST_HOME)/lib -L${LIBRARY_OUTPUT_DIR}
TESTS_DEPS := build-library
