#include "libcalc.h"
#include "gtest/gtest.h"
#include <math.h>

/**
 * @file
 */

#define N 10 /**< second operator array size */
#define STARTING_NUMBER 5.0 /**< number with which is calculator initialized */

using namespace std;

/**
 * @brief This class is an environment for the testing of the mathematical
 * library, it contains the data (numbers) set on which is the functionality
 * tested. With each test is the environment recreated for better reliability.
 */
class CalcFunctionsTesting : public ::testing::Test {
	public:
        Calculator *c;
        CommandFactory *factory; 

		/** Data set for testing */
        double y_arr[N] = {10.0, -236.0, 0.0, 1.0, -1.0, -3.1415, 1256.578, 1.00001, 69.00, 42.0};
        double nth_power_base_arr[N] = {3.0, -3.0, 3.0, 0.0, 0.0, 15.0, 69.0, 0.24, 0.00042, -23.48};
        double nth_power_arr[N] =  {3.0, 2.0, -2.0, 0.0, -2.3, 3.14, 56.0, 0.0 , 15.2, 0.5};
        double nth_root_base_arr[N] = {-45.0, -123.2314, -0.0, 0.0, 10.389, 68.999000, 144.144, 144.0, 99.0, 3.1450};
        double nth_root_arr[N] = {1.324, 0.0, 2.0, 3.123, 0.0, 0.0, 1.0, 9.6, 12.23, 3.0}; 
        double nth_log_base_arr[N] = {1.0, 2.0, 0.213, 5.0, 42.0, 0.2315, 12.0, 55.6978, 123.999, 123.999}; 
        double nth_log_arr[N] = {-12.0, -4.23223, 0.0, 1.0, 1.00023, 0.123, 144.0, 420.0, -123.999, 123.999};

    protected:
        virtual void SetUp() {
       		c = new Calculator(STARTING_NUMBER);
            factory = new CommandFactory(c);
        }
        
        typedef Command::CommandType CommandType;
    
        /**
         * @brief function that decides if number has decimal part or not
         * @param double    num    number that one wants to evaluate
         * @returns int            1 - is a double
         *                         0 - not a double
         */
        int isDouble(double num){
           return floor(num) != num;
        }

        virtual void TearDown(){
            delete c;
            delete factory;
        }
};

/**
 * @defgroup Tests for mathematical library
 * @{
 */


/** 
 * Testing getCurrentState function. 
 *
 * @note Using ASSERT because other tests depend on its proper functionality.
 */
TEST_F(CalcFunctionsTesting, getCurrentState){
   ASSERT_EQ(c->getCurrentState(), STARTING_NUMBER);
}

/**
 * Testing SetCalculatorValue command creation and proper application.
 *
 * @note Using ASSERT because other tests depend on proper functionality of this command type.
 */
TEST_F(CalcFunctionsTesting, SetCalculatorValueCommand){
	double oldValue = c->getCurrentState();

	Command cmd = factory->createCommand(CommandType::SetCalculatorValue, 0);
	c->applyCommand(cmd);

	ASSERT_EQ(c->getCurrentState(), 0);
	ASSERT_NE(c->getCurrentState(), oldValue);
}

/** 
 * Testing NoOperation command creation and proper application.
 *
 * @brief Testing consists of N tests. In each iteration a NoOperation command 
 * is created and applied to a value from y_arr.
 */
TEST_F(CalcFunctionsTesting, NoOperationCommand){
    double oldValue = c->getCurrentState();

    for ( int i = 0; i < N; i++ ){
	    Command cmd = factory->createCommand(CommandType::NoOperation, y_arr[i]);
	    c->applyCommand(cmd);

        EXPECT_EQ(c->getCurrentState(), oldValue);
    }
}

/**
 * Addition test
 *
 * @brief Testing Add command creation and proper application. Testing consist
 * of N tests. Each iteration takes last value from calculator and adds it to
 * a number from y_arr array and tests the result. 
 */
TEST_F(CalcFunctionsTesting, AddCommand){
    double x, result;

    for ( int i = 0; i < N; i++ ){
        x = c->getCurrentState();
         
        Command cmd = factory->createCommand(CommandType::Add, y_arr[i]);
        c->applyCommand(cmd);

        result = x + y_arr[i];
        
        EXPECT_EQ(c->getCurrentState(), result);
    }
    
    double sum = STARTING_NUMBER;
    for ( int i = 0; i < N; i++ ){
        sum += y_arr[i]; 
    }
    EXPECT_EQ(sum,result);

}

/**
 * Subtraction test
 *
 * @brief Testing Sub command creation and proper application. Testing consist 
 * of N tests. Each iteration takes last value from calculator and subtracts 
 * it from a number from y_arr array and tests the result. 
 */
TEST_F(CalcFunctionsTesting, SubCommand){
    double x, result;

    for ( int i = 0; i < N; i++ ){
        x = c->getCurrentState();

        Command cmd = factory->createCommand(CommandType::Sub, y_arr[i]);
        c->applyCommand(cmd);

        result = x - y_arr[i];

        EXPECT_EQ(c->getCurrentState(), result);
    }
}

/**
 * Multiplication test
 *
 * @brief Testing Mul command creation and proper application. Testing consist
 * of N tests. Each iteration takes last value from calculator and multiplies
 * it by a number from y_arr array and tests the result. 
 */
TEST_F(CalcFunctionsTesting, MulCommand){
    double x, result;

    for ( int i = 0; i < N; i++ ){
        x = c->getCurrentState();

        Command cmd = factory->createCommand(CommandType::Mul, y_arr[i]);
        c->applyCommand(cmd);

        result = x * y_arr[i];

        EXPECT_EQ(c->getCurrentState(), result);
    }
}

/**
 * Division test
 *
 * @brief Testing Div command creation and proper application. Testing consist
 * of N tests. Each iteration takes last value from calculator, divides it by
 * a number from y_arr array and tests the result.
 */
TEST_F(CalcFunctionsTesting, DivCommand){
    double x, result;

    for ( int i = 0; i < N; i++ ){
        x = c->getCurrentState();

        if ( y_arr[i] == 0.0 ){

            /** 
             * Expecting an error if division by zero occurs. Example: 5/0
             */
            EXPECT_ANY_THROW(factory->createCommand(CommandType::Div, y_arr[i]));
        }
        else {
            Command cmd = factory->createCommand(CommandType::Div, y_arr[i]);
            c->applyCommand(cmd);

            result = x / y_arr[i];

            EXPECT_EQ(c->getCurrentState(), result);
        }
    }
}

/**
 * Nth power test
 *
 * @brief Testing NthPower command creation and proper application. Testing
 * consist of N tests. Iterates throught two arrays, rising a value from the
 * first (nth_power_base_arr) array to the value from the second (nth_power_arr) array.
 */ 
TEST_F(CalcFunctionsTesting, NthPowerCommand){
    double result;

    for ( int i = 0; i < N; i++ ){
        Command cmd = factory->createCommand(CommandType::SetCalculatorValue, nth_power_base_arr[i]);
	    c->applyCommand(cmd);        
        EXPECT_EQ(nth_power_base_arr[i],c->getCurrentState());

        if ( nth_power_base_arr[i] < 0 && isDouble(nth_power_arr[i]) ){

            /**
             * Expecting an error if rising a negative number to power of 
             * fraction number. Example: -5^{1/2}
             */
            EXPECT_ANY_THROW(factory->createCommand(CommandType::NthPower, nth_power_arr[i]));
        }
        else if ( nth_power_base_arr[i] == 0 && nth_power_arr[i] < 0){

            /**
             * Expecting error if rising 0 to power of negative number. Example: 0^{-2}
             */
            EXPECT_ANY_THROW(factory->createCommand(CommandType::NthPower, nth_power_arr[i]));
        }
        else{
            Command cmd = factory->createCommand(CommandType::NthPower, nth_power_arr[i]);
	        c->applyCommand(cmd);

            result = pow(nth_power_base_arr[i], nth_power_arr[i]);

            EXPECT_EQ(c->getCurrentState(), result);
        }
    }
}

/**
 * Nth root test
 *
 * @brief Testing NthRoot command creation and proper application. Testing 
 * consist of N tests. Iterates throught two arrays, calculates n-th root of 
 * value from the first (nth_root_base_arr) array, with the n beeing the value 
 * from the second (nth_pow_arr) array.
 */
TEST_F(CalcFunctionsTesting, NthRootCommand){
    double result;

    for ( int i = 0; i < N; i++ ){
        Command cmd = factory->createCommand(CommandType::SetCalculatorValue, nth_root_base_arr[i]);
	    c->applyCommand(cmd);
        EXPECT_EQ(nth_root_base_arr[i], c->getCurrentState());
        
        if ( nth_root_base_arr[i] < 0 || nth_root_arr[i] == 0 ){

            /**
             * Expecting an error if:
             *              root of negative number occurs. Example: \sqrt{-3}
             *              0th root of any number occurs. Example: \sqrt[0]{3} 
             */
            EXPECT_ANY_THROW(factory->createCommand(CommandType::NthRoot, nth_root_arr[i]));
        } 
        else{
            Command cmd = factory->createCommand(CommandType::NthRoot, nth_root_arr[i]);
	        c->applyCommand(cmd);

            result = pow( nth_root_base_arr[i], (1.0 / nth_root_arr[i]) );

            EXPECT_EQ(c->getCurrentState(), result);
        }
    }
}

/**
 * Factorial test
 *
 * @brief Testing Factorial command creation and proper application.
 * Testing consist of N tests. Iterates through y_arr array,
 * claculates factorial of its values and tests them with result
 * of separate calculation.
 */
TEST_F(CalcFunctionsTesting, FactorialCommand){
    double result;

    for ( int i = 0; i < N; i++ ){
        Command cmd = factory->createCommand(CommandType::SetCalculatorValue, y_arr[i]);
	    c->applyCommand(cmd);
        EXPECT_EQ(y_arr[i], c->getCurrentState());
        
        /** @Note: It is possible to take factorial of zero which is defined as 0!=1 */
        if ( isDouble(y_arr[i]) || y_arr[i] < 0 ){
            /**
             * Expecting an error if: 
             *          factorial of fraction number occurs. Example: 1.2!
             *          factorial of negative number or zero  occurs. Example: -1!
             */
            
            EXPECT_ANY_THROW(factory->createCommand(CommandType::Factorial, y_arr[i]));
        }
        else{
            Command cmd = factory->createCommand(CommandType::Factorial, y_arr[i]);
            c->applyCommand(cmd);
            
            result = 1.0; /**< Initial state (neutral number for multiplication) */

            for ( int num = y_arr[i]; num > 1; num-- ){
                result *= num;
            }

            EXPECT_EQ(c->getCurrentState(), result);
        }
    }
}

/* GenericLogarithm test
 *
 * @brief Testing NthLog command creation and proper application. Testing 
 * consist of N tests. Iterates throught two arrays, applying n-th logarithm
 * to a value from the first (nth_log_base_arr) array with the n beeing the 
 * value from the second (nth_log_arr) array.
 */
TEST_F(CalcFunctionsTesting, NthLogCommand){
    double result;
    
    for ( int i = 0; i < N; i++ ){
        Command cmd = factory->createCommand(CommandType::SetCalculatorValue, nth_log_base_arr[i]);
	    c->applyCommand(cmd);
        EXPECT_EQ(nth_log_base_arr[i], c->getCurrentState());
        
        /** 
         * @note base in array variable name might be confusing 
         * (it is used this way to maintain similarity with root and power)
         */
        if (nth_log_base_arr[i] <= 0 || nth_log_arr[i] == 1 || nth_log_arr[i] <= 0){

            /**
             * Expecting an error if one tries to apply:
             *          a logarithm on negative number. Example: \log{-3}
             *          1th logarithm. Example: \log[-2]{8}
             *          [insert negative number]th logarithm. Example: \log[1]{5}
             */ 
            EXPECT_ANY_THROW(factory->createCommand(CommandType::Log, nth_log_arr[i]));
        } 
        else {
            Command cmd = factory->createCommand(CommandType::Log, nth_log_arr[i]);
	        c->applyCommand(cmd);

            result = ( log(nth_log_base_arr[i]) / log(nth_log_arr[i]) ); 

            EXPECT_EQ(c->getCurrentState(), result);
        }
    }
}

/**
 * History test
 *
 * @brief Creates some traffic to fill up history buffer and also creates its 
 * own. After tests historyForward and historyBackward functions on it. 
 * Also tests if overflow errors are set.
 */
TEST_F(CalcFunctionsTesting, history){
    
    /**
     * At the begining one shouldn't be able to go backward/forward in history
     */
    EXPECT_ANY_THROW(c->historyBackward());
    EXPECT_ANY_THROW(c->historyForward());

    std::vector<double> history = std::vector<double>();
    history.resize(N+1); /** @note We need +1 value to accomodate intial state. */

    for (int i = 0; i < N; i++){
        history[i] = c->getCurrentState();

        Command cmd = factory->createCommand(CommandType::Add, y_arr[i]);
        c->applyCommand(cmd);
    }

    history[N] = c->getCurrentState();
    
    for (int i = N-1; i >= 0; i--){
        /** @note using ASSERT because rest of this test dependos on this. */
        ASSERT_EQ(c->historyBackward(), history[i]);
    }

    /** 
     * Going backwards even further shouldn't be possible.
     */
    EXPECT_ANY_THROW(c->historyBackward());
    EXPECT_EQ(c->getCurrentState(), STARTING_NUMBER);

    /** 
     * @note The history_arr[0] is the initial state of calculator.
     * This will never be returned by historyForward
     */
    for (int i = 1; i <= N; i++){
        EXPECT_EQ(c->historyForward(), history[i]);
    }

    /**
     * Going forwards even further shouldn't be possible.
     */
    EXPECT_ANY_THROW(c->historyForward());
}

/**
 * @}
 */
