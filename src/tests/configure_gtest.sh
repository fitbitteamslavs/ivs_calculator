#!/bin/bash

GTEST_URL='https://github.com/google/googletest.git'

echo "Creating gtest configuration file.."
if [[ "$GTEST_HOME" ]]; then
    cat << EOF > "$PROJECT_ROOT"/gtest_config.mk
GTEST_HOME:=$GTEST_HOME
DEFINITIONS += \$(SOURCES:.cpp=.d)
EOF
else
    if [[ -d "$PROJECT_ROOT"/googletest ]]; then
        cat << EOF > "$PROJECT_ROOT"/gtest_config.mk
GTEST_HOME:=$PROJECT_ROOT/googletest
DEFINITIONS += \$(SOURCES:.cpp=.d)
EOF
    exit 0;
    fi

    echo "Enviromental variable \$GTEST_HOME not set, however GTest is required to compile tests."
    read -p "Would you like to download it? y/n: " confirm
    if [[ "$confirm" = "y" ]]; then
        git clone --depth 1 "$GTEST_URL" "$PROJECT_ROOT"/googletest
        if [[ ! $? -eq 0 ]]; then
            echo "GoogleTest could not be downloaded." 1>&2
            exit 1
        else 
            cd "$PROJECT_ROOT"/googletest
            cmake .
            make
            cat << EOF > "$PROJECT_ROOT"/gtest_config.mk
GTEST_HOME:=$PROJECT_ROOT/googletest
DEFINITIONS += \$(SOURCES:.cpp=.d)
EOF
        fi
    else
        echo "Tests cannot be compiled without GTest" 1>&2
        echo "If you have googletest installed, set environmental variable GTEST_HOME to its location." 1>&2
        exit 1
    fi
fi




