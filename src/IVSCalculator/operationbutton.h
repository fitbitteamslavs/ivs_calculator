#ifndef OPERATIONBUTTON_H
#define OPERATIONBUTTON_H

#include "squarebutton.h"
#include "libcalc.h"
/**
  * @file
  */
/**
 * @ingroup GUI
 * @brief The OperationButton class Is class for all operation buttons.
 */
class OperationButton : public SquareButton
{
    Q_OBJECT
public:
    /**
     * @brief Sets font size to 20
     * @param parent Parent widget
     */
    OperationButton(QWidget *parent = nullptr);
    ~OperationButton() {};

    /**
     * @brief Sets operation type. Used in files generated from .ui
     * @param type Command type of given button
     */
    void setType(Command::CommandType type);
    /**
     * @brief Returns operation type of the button
     * @return Operation type of the button
     */
    Command::CommandType getType();
    /**
     * @brief Sets the button as unary, default is false. Used in files generated from .ui
     * @param isUnary True if the button should be unary, else false.
     */
    void setIsUnary(bool isUnary);

private slots:
    /**
     * @brief Emits clicked signal, but with required data.
     * @details Required data are:
     *          * Operation type of type @ref Command::CommandType encoded in int
     *          * Operation String reprezentation (from button text)
     *          * Operation family - unary(true) or binary(false)
     */
    virtual void emitWithData() override;

private:
    /**
     * @brief Button operation type
     */
    Command::CommandType type;
    /**
     * @brief true if button is unary else false
     */
    bool isUnary = false;
};

#endif // OPERATIONBUTTON_H
