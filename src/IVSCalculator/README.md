# Environment Setup
-----------------
After opening .pro file in QTCreator go to Projects in the left bar and under Code Style import file `.qtcodestyle.xml` which is located in this directory.
In Build settings set build directory to `(Project root)/build`. This will be required for all versions (Debug, Profile, Release).
## Optional
If you want to use vim emulator enable it in `Tools/Options/FakeVim`
