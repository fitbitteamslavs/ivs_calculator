#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QKeyEvent>
#include <QWizard>
#include <QLabel>

#include "libcalc.h"
#include "squarebutton.h"
#include "numberbutton.h"
#include "operationbutton.h"
#include "helpwizard.h"

/** @mainpage Conputer
 * @section Intro
 * Welcome to the documentation for our IVS project of calculator
 * There are two modules @ref GUI and @ref Library.
 * In @ref CalculatorGUI "GUI pages" you can find definition and design of GUI and basic
 * operation. In @ref Library you can find the interface of our calculator
 * library and architecture.
 * @image html screenshot.png
 */

/**
 * @file
 * @page CalculatorGUI Conputer GUI
 * @ingroup GUI
 * @section Introduction
 * Here is a simplified table that shows how the GUI is organized.
 * <table class="doxtable" width="500">
 * <caption>%Conputer simplified GUI</caption>
 * <tr><th colspan="10" style="text-align:left">%Conputer
 * <tr><td colspan="10" style="text-align:right"> Previous value
 * <tr><td colspan="10" style="text-align:right"> Operation label
 * <tr><td colspan="10" style="text-align:right"> Current value
 * <tr> <td rowspan="2">
 *      <table style="doxtable" width="300">
 *          <tr><td>7<td>8<td>9
 *          <tr><td>4<td>5<td>6
 *          <tr><td>1<td>2<td>3
 *          <tr><td>+/-<td>0<td>.
 *      </table>
 *      Number Section
 *      <td> <table style="doxtable" width="100">
 *          <tr><td>CE<td>C<td>&lt;=<td>↶<td>↷
 *           </table>
 *           Edit section
 * <tr> <td colspan="6">
 *      <table style="doxtable" width="300">
 *          <tr><td>+<td>-<td>x<sup>2</sup><td>x<sup>n</sup>
 *          <tr><td>*<td>/<td>√x<td>x<sup>n</sup>
 *          <tr><td colspan="2">=<td>!<td>log<sub>n</sub>x
 *      </table>
 *      Operation Section
 * </table>
 * @subsection numbers Number section
 * Here are all number buttons, but also decimal point and sign switch. If you want to change the sign use this
 * instead of <i>'-'</i> from @ref operations as that will add an operation to the history.
 * @subsection edit Edit section
 * Here are buttons:
 *      - C = Clear all and reset calculator (except history) to default state
 *      - CE = Clear current number, keep operation
 *      - &lt;= = Remove one character, acts as Backspace
 *      - Back and forward arrows = move in history
 * @subsection operations Operation section
 * Here are different opearions. The calculator acts as one would expect. First user enters first number, than some operation
 * and second number. Then user can click equals sign or continue with another operation. Mind that first operand will ALWAYS be
 * the result of previous operation ie. no sign precedence applies here. <br>
 * There are also unary operators, results of these operations are always displayed in Current Value:
 *      - x<sup>2</sup> = square value
 *      - ! = factorial
 *      - √x = takes square root <br>
 *
 * The log<sub>n</sub>x first operand is <i>x</i> and second (after click on operation) is the base <i>n</i>
 *
 */
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

#define WIZARD_WIDTH 400
/**
 * @defgroup GUI Graphical interface
 */

/**
 * @brief The MainWindow class
 * @ingroup GUI
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * @brief Constructor for main window
     * @param parent parent widget pointer, as root widget this should be nullptr
     * @details Connects number and operation buttons to local slots. Sets window title and binds keyboard.
     */
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected:
    /**
     * @brief Virtual function to connect events from keyboard ie. key press.
     * @param event Contains event details such as pressed key number and character.
     * @details Catches numbers keys, period, Enter is interpreted as equals,
     * Backspace removes one character from the end of the current number.
     * Accepted operation symbols are: '+', '-', '*', '/', '!' for factorial and '^' for N-th power.
     * If other key is pressed it is forwarded to default handler @link QMainWindow::keyPressEvent() @endlink
     */
    virtual void keyPressEvent(QKeyEvent *event);

private slots:
    /**
     * @brief Disables or enables all buttons in the window, except C, CE, and <=
     * @param state true if enable, false to disable
     */
    void setEnabledAll(bool state = false);

    /**
     * @brief Toggles number section buttons
     * @param enable true if enable, false to disable
     */
    void enableNumbers(bool enable);
    /**
     * @brief Toggles plus button
     * @param enable true if enable, false to disable
     */
    void enableAdd(bool enable);
    /**
     * @brief Toggles Second power button
     * @param enable true if enable, false to disable
     */
    void enableSecPow(bool enable);
    /**
     * @brief Toggles Equals button
     * @param enable true if enable, false to disable
     */
    void enableEquals(bool enable);
    /**
     * @brief Toggles history Undo and Redo buttons
     * @param enable true if enable, false to disable
     */
    void enableHistory(bool enable);

    /**
     * @brief Draws rectangles of different colors around individual sections (ie. Numpad, operations...)
     * @param high true if highlight, false to disable
     * @details This function is used for Help/Tutorial only
     */
    void highlightAreas(bool high);
    /**
     * @brief Removes all restrictions and changes to the UI for the Help/Tutorial
     */
    void helpFinished();

    /**
     * @brief Called when equals button is pressed.
     * @details Operation label is set to '=' and next operation is set to @link Command::SetCalculatorValue @endlink
     * Result is printed on the main input.
     */
    void on_equals_clicked();

    /**
     * @brief Toggles sign on current number.
     * @details If the number is negative removes '-' sign and if it is positive it adds '-' to the beggining.
     */
    void on_changeSign_clicked();

    /**
     * @brief Clears only currently entered number.
     */
    void on_delCurrent_clicked();

    /**
     * @brief Removes one character from the end of number. Calls @link DisplaySection::backspace @endlink
     */
    void on_delChar_clicked();

    /**
     * @brief Clears user view. Calls @link DisplaySection::clearAll @endlink
     */
    void on_delAll_clicked();

    /**
     * @brief Slot for when undo button is clicked
     * @details There is constant length history buffer in library, this calls @link Calculator::historyBackwards @endlink
     * and currently entered number is set to the return value.
     */
    void on_undo_clicked();

    /**
     * @brief Slot for redo button
     * @details Works similar to @link on_undo_clicked(); @endlink but forwards in history.
     */
    void on_redo_clicked();
    void on_helpButton_clicked();

public slots:
    /**
     * @brief Slot for number section buttons, adds clicked number to the currently entered number.
     * @param data QVariant<QString> with the text from buttons
     */
    void numberClicked(QVariant &data);
    /**
     * @brief Slot for operation button click.
     * @param operation Contains QVariant<QVariantList> as data. The order of values is strictly given @see OperationButton::emitWithData
     */
    void operationClicked(QVariant &operation);

private:
    /**
     * @brief Helper function for communicating with the library.
     * @param value Value from currently entered number (the operation will be sent with this number)
     * @return Pair of values .first is the result of the library and .second is true if no error was thrown else false
     * @warning Always sets the @ref currentOperation to Command::SetCalculatorValue
     */
    QPair<double, bool> sendOperation(double value);

    Ui::MainWindow *ui;

    /**
     * @brief Library @ref Calculator instance.
     */
    Calculator backend;
    /**
     * @brief Library @ref CommandFactory instance.
     */
    CommandFactory factory;

    /**
     * @brief Next operation to be performed
     * @details Since we need delay, because after clicking operation button second operand needs to be enetered but to the
     * library second operand is send with the clicked operation.
     */
    int currentOperation = Command::SetCalculatorValue;
};
#endif // MAINWINDOW_H
