#ifndef SQUAREBUTTON_H
#define SQUAREBUTTON_H

/**
  * @file
  */

#include <QPushButton>
#include <QToolButton>
#include <QDebug>
#include <QPainter>
#include <QTextDocument>

/**
 * @ingroup GUI
 * @brief The SquareButton class is parent for all buttons
 * @details Defines universal functions (mainly for keeping the buttons square) but also
 * signals for communication with @ref MainWindow and Overrides @ref QToolButton::setText
 * to support html formatting.
 */
class SquareButton : public QToolButton
{
Q_OBJECT
public:
    /**
     * @brief Sets sizes and signal forwarding from clicked to @ref clickedWithData
     * @param parent Parent widget pointer
     */
    SquareButton(QWidget *parent = nullptr);
    ~SquareButton(){};

    /**
     * @brief Sets font size of button
     * @param size Font size in pts
     */
    void setFontSize(int size);

    virtual bool hasHeightForWidth() const;
    virtual int heightForWidth(int w) const;
    virtual int minimumHeightForWidth(int w);

    /**
     * @brief Overrides default function to support html formatting.
     * @param str Html text to be rendered on the button
     * @details Generates QPixmap with rendered given html text.
     */
    virtual void setText(const QString &str);

signals:
    /**
     * @brief Is emitted when button is clicked.
     * @param data Depends on child implementation
     */
    void clickedWithData(QVariant &data);

private slots:
    /**
     * @brief Slot for forwarding clicked() event with data. Emits @ref<clickedWithData>[a]
     */
    virtual void emitWithData();

protected:
    /**
     * @brief Font size in pts. Set by @ref setFontSize
     */
    int fontSize = 15;
};

#endif // SQUAREBUTTON_H
