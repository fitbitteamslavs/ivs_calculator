#ifndef DISPLAYSECTION_H
#define DISPLAYSECTION_H

#include <QFrame>
#include <QDebug>
/**
 * @file
 */

namespace Ui {
class DisplaySection;
}

/**
 * @ingroup GUI
 * @brief The DisplaySection class handles communication with user: Displays results, operations and currently entering number.
 */
class DisplaySection : public QFrame
{
    Q_OBJECT

public:
    explicit DisplaySection(QWidget *parent = nullptr);
    ~DisplaySection();

    /**
     * @brief Sets Previous number field (the upper one) to given number
     * @param Number to set to.
     */
    void setPrevNum(double num);

    /**
     * @brief Special function for handling results from @ref MainWindow::sendOperation and printing results
     * @param operationRepr Operation text (label in between the Current Number and Previous Number fields
     * @param newValue Pair of new value and boolean representing if it is valid
     * @param keepPrevVal If the value should be set to Previous Number (true) and Current Number cleared or Current number (false) and Previous Number cleared
     */
    void setOperation(QString operationRepr, QPair<double, bool> newValue, bool keepPrevVal = true);

    /**
     * @brief Sets Current Number field to given number
     * @param num Number to set to.
     */
    void setCurrentNum(double num);
    /**
     * @brief Print message to user, it is removed on next change to any fields
     * @param msg Message to print
     */
    void printMessage(QString msg);
    /**
     * @brief Usually called when number is pressed. Adds given digit to Current Number
     * @param c Character to add
     */
    void addDigitToNumber(QChar c);
    /**
     * @brief Negates (adds or removes '-' from the beggining of) Current Number
     */
    void negate();
    /**
     * @brief Removes one (most recently added) character from the end of the Current Number
     */
    void backspace();

    /**
     * @brief Returns Current Number value
     * @return Value from Current Number field as double
     * @see getCurrentNumAsString
     */
    double getCurrentNum();
    /**
     * @brief Returns Current Number as string
     * @return String from Current Number
     * @see getCurrentNum
     */
    QString getCurrentNumAsString();

    /**
     * @brief Clears all fields: Current Number, Operation label and Previous Number
     */
    void clearAll();
    /**
     * @brief Clears only Current Number
     */
    void clearCurrent();

private:
    /**
     * @brief Helper function for setting Previous Number
     * @param str String to set to
     */
    void setPrevNum(QString str);

    /**
     * @brief Helper function for setting Current Number
     * @param str String to set to
     */
    void setCurrentNum(QString str);

    Ui::DisplaySection *ui;

    /**
     * @brief Helper variable. Set to true on calling @ref printMessage and reset on any other calling of writing function.
     */
    bool clearOnNextInput = false;
};

#endif // DISPLAYSECTION_H
