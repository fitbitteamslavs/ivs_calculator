#include "helpwizard.h"
#include "ui_helpwizard.h"

HelpWizard::HelpWizard(QWidget *parent) :
    QWizard(parent),
    ui(new Ui::HelpWizard)
{
    ui->setupUi(this);

    this->setWindowFlags(Qt::Widget);
    this->setWindowModality(Qt::NonModal);
    this->setDisabled(true);
    this->setHidden(true);
    connect(this, &QWizard::currentIdChanged, this, &HelpWizard::doActions);
    prevId = -1;

}

HelpWizard::~HelpWizard()
{
    delete ui;
}

void HelpWizard::doActions(int actId)
{
    // If going forwards with next, then actId > prevId
    // If going backwards the operations are the same but inversed
    // Cases are shifted one forward because actId is the id of the "switched to" page
    // Which means that if we are going forward the cases contain actions to perform on
    // the current page
    // When going backwards (sice all transitions are linear, ie. next page's id is currentId + 1)
    // each page can tidy up its configuration when bools are negated, so when we go back
    // the page following current one, it can tidy up its own configuration
    // The bool values before commands represent values that should be passed when going forward
    // ie. clicking Next
    bool enable = actId > prevId;
    switch (actId > prevId ? actId : actId+1) {
    case Sections:
        //true
        emit highlightAreas(enable);
        break;
    case Add1:
        //false
        emit highlightAreas(!enable);
        //true
        emit enableNums(enable);
        break;
    case Add4:
    case Add2:
        //false
        emit enableNums(!enable);
        //true
        emit enableAdd(enable);
        break;
    case Add5:
    case Add3:
        //true
        emit enableNums(enable);
        //false
        emit enableAdd(!enable);
        break;
    case Add6:
        //false
        emit enableNums(!enable);
        //true
        emit enableEquals(enable);
        break;
    case Pow1:
        //false
        emit enableEquals(!enable);
        //true
        emit enableNums(enable);
        break;
    case Pow2:
        //false
        emit enableNums(!enable);
        //true
        emit enableSecPow(enable);
        break;
    case History:
        //false
        emit enableSecPow(!enable);
        //true
        emit enableHistory(enable);
        break;
    default:
        break;
    }
    prevId = actId;
}
