#ifndef NUMBERBUTTON_H
#define NUMBERBUTTON_H

#include "squarebutton.h"

/**
  * @file
  */

/**
 * @ingroup GUI
 * @brief The NumberButton class Is class for all numbered buttons and '.' button
 */
class NumberButton : public SquareButton
{
Q_OBJECT
public:
    /**
     * @brief Sets default font size to 40
     * @param parent Parent widget
     */
    NumberButton(QWidget *parent = nullptr);
    ~NumberButton() {};

private slots:
    /**
     * @brief Emits @ref clickedWithData with button text
     */
    virtual void emitWithData() override;

};

#endif // NUMBERBUTTON_H
