#include "operationbutton.h"

OperationButton::OperationButton(QWidget *parent) : SquareButton(parent)
{
    this->fontSize = 20;
    this->setProperty("isUnary", this->isUnary);
}

void OperationButton::setType(Command::CommandType type)
{
    this->type = type;
}

Command::CommandType OperationButton::getType()
{
    return this->type;
}

void OperationButton::setIsUnary(bool isUnary)
{
    this->isUnary = isUnary;
    this->setProperty("isUnary", this->isUnary);
}

void OperationButton::emitWithData()
{
    QList<QVariant> list;
    list.append(this->type);
    list.append(this->text());
    list.append(this->isUnary);
    QVariant msg(list);
    emit this->clickedWithData(msg);
}
