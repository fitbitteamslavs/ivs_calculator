#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow), backend(0.0), factory(&backend)
{
    ui->setupUi(this);
    this->setWindowIcon(QIcon("icon_ivscalc.png"));

    // Connect signals for the Help/Tutorial
    connect(ui->helpWizard, &HelpWizard::highlightAreas, this, &MainWindow::highlightAreas);
    connect(ui->helpWizard, &HelpWizard::finished, this, &MainWindow::helpFinished);
    connect(ui->helpWizard, &HelpWizard::enableAdd, this, &MainWindow::enableAdd);
    connect(ui->helpWizard, &HelpWizard::enableEquals, this, &MainWindow::enableEquals);
    connect(ui->helpWizard, &HelpWizard::enableSecPow, this, &MainWindow::enableSecPow);
    connect(ui->helpWizard, &HelpWizard::enableNums, this, &MainWindow::enableNumbers);
    connect(ui->helpWizard, &HelpWizard::enableHistory, this, &MainWindow::enableHistory);

    // Connect number buttons to one slot -> numberClicked()
    for(int i = 0; i < ui->numPad->count() ;i++){
        QWidget *w = ui->numPad->itemAt(i)->widget();
        // Gets the name of the class of given element in layout and checks if it is NumberButton
        // QString(typeid(*w).name()).contains(QStringLiteral("NumberButton"))
        if(w != ui->changeSign){
            QObject::connect((NumberButton*) w, &NumberButton::clickedWithData, this, &MainWindow::numberClicked);
        }
    }

    // Connect operation buttons to one slot -> operationClicked
    for(int i = 0; i < ui->operationPad->count(); i++){
        QWidget *w = ui->operationPad->itemAt(i)->widget();
        if(w != ui->equals) {
            QObject::connect((OperationButton*) w, &OperationButton::clickedWithData, this, &MainWindow::operationClicked);
        }
    }

    this->setWindowTitle("Calculator");

    // Style for error messages on the bottom
    ui->statusbar->setStyleSheet("QStatusBar {color: red;}");

    // For capturing input from the keyboard too
    this->grabKeyboard();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    // If conversion to number from string was successful
    bool OK;
    int num = event->text().toInt(&OK);
    QVariant data;
    if(OK){
        data.setValue(num);
        this->numberClicked(data);
    } else if(event->text() == ".") {
        data.setValue(QString("."));
        this->numberClicked(data);
    } else if(event->key() == Qt::Key::Key_Enter || event->key() == Qt::Key::Key_Return){
        this->on_equals_clicked();
    } else if(event->key() == Qt::Key::Key_Backspace){
        this->on_delChar_clicked();
    } else if(!event->text().isEmpty() && QString("+-*/!^").contains(event->text())){
        switch(event->text().at(0).toLatin1()){
            case '+': ui->plus->click();break;
            case '-': ui->minus->click();break;
            case '*': ui->times->click();break;
            case '/': ui->divide->click();break;
            case '!': ui->factorial->click();break;
            case '^': ui->nPower->click();break;
        }
        qDebug() << "Operation: " << event->text();
    }
    QMainWindow::keyPressEvent(event);
}


void MainWindow::on_equals_clicked()
{
    ui->display->setOperation("=", sendOperation(ui->display->getCurrentNum()), false);
    this->currentOperation = Command::SetCalculatorValue;
    qDebug() << "Equals clicked";
}

void MainWindow::numberClicked(QVariant &data)
{
    if(!ui->display->getCurrentNumAsString().contains('.') || data.toString() != ".")
        ui->display->addDigitToNumber(data.toString().at(0));
}

void MainWindow::operationClicked(QVariant &operation)
{
    ui->statusbar->clearMessage();

    bool isUnary = operation.toList().at(2).toBool();
    QPair<double, bool> result = sendOperation(ui->display->getCurrentNum());

    if(isUnary){
        /* For unary operations there needs to be two transactions, first completes previous operator
         * and second executes unary operator. Because of this there would be a gap in operators sequence.
         * Thus next time NoOperation will be send to compensate.
         */
        if(!result.second) {
            return;
        }

        this->currentOperation = operation.toList().at(0).toInt();

        // 2 is here to workaround button "power of two" as unary operator
        // other unary operations ignore the second operand, so no harm here
        // until other operations are added
        result = sendOperation(2);

        // Set the result to the Currently entered number, not previous
        ui->display->setOperation(operation.toList().at(1).toString(), result, false);

    } else {
        ui->display->setOperation(operation.toList().at(1).toString(), result);

        if(result.second)
            this->currentOperation = operation.toList().at(0).toInt();
    }
}

void MainWindow::setEnabledAll(bool state)
{
    enableNumbers(state);
    for(int i = 0; i < ui->operationPad->count(); i++){
        ui->operationPad->itemAt(i)->widget()->setEnabled(state);
    }
    ui->delAll->setEnabled(state);
    ui->undo->setEnabled(state);
    ui->redo->setEnabled(state);
}

void MainWindow::enableNumbers(bool enable)
{
    for(int i = 0; i < ui->numPad->count(); i++){
        ui->numPad->itemAt(i)->widget()->setEnabled(enable);
    }
}

void MainWindow::enableAdd(bool enable)
{
    ui->plus->setEnabled(enable);
}

void MainWindow::enableSecPow(bool enable)
{
    ui->square->setEnabled(enable);
}

void MainWindow::enableEquals(bool enable)
{
    ui->equals->setEnabled(enable);
}

void MainWindow::enableHistory(bool enable)
{
    ui->undo->setEnabled(enable);
    ui->redo->setEnabled(enable);
}

void MainWindow::highlightAreas(bool high)
{
    if(high){
        // Colors correspond to the HelpWizard Sections formatting
        ui->display->setStyleSheet("#display {border: 2px solid black}");
        ui->numPadW->setStyleSheet("#numPadW {border: 2px solid #00aa00}");
        ui->editPadW->setStyleSheet("#editPadW {border: 2px solid #ff0000}");
        ui->operationPadW->setStyleSheet(" #operationPadW {border: 2px solid #0055ff}"
                                         " OperationButton[isUnary=\"true\"] {border: 2px solid #aa00ff}"
                                         " OperationButton[isUnary=\"false\"] {border: 2px solid #b9530a}");
    } else {
        ui->display->setStyleSheet("");
        ui->numPadW->setStyleSheet("");
        ui->editPadW->setStyleSheet("");
        ui->operationPadW->setStyleSheet("");

    }
}

void MainWindow::helpFinished()
{
    this->setMaximumWidth(this->maximumWidth()-WIZARD_WIDTH);
    QSize newSize(this->maximumWidth(), height());
    this->resize(newSize);

    ui->helpButton->setEnabled(true);
    this->setEnabledAll(true);
    highlightAreas(false);
}

QPair<double, bool> MainWindow::sendOperation(double value)
{
    double res = 0; // result of the operation from the backend
    bool valid = true;
    try {
        Command com = factory.createCommand((Command::CommandType) currentOperation, value);
        res = this->backend.applyCommand(com);
    } catch (const std::invalid_argument &exc) {
        valid = false;
        ui->display->printMessage("INVALID INPUT");
        ui->statusbar->showMessage(exc.what());
    } catch (const std::exception &exc) {
        valid = false;
        ui->display->printMessage("ERR");
        ui->statusbar->showMessage(exc.what());
    }

    if(qIsInf(res)) {
        ui->statusbar->showMessage("Result is out of range, using infinity. No operations with infinity possible.");
    } else if(qIsNaN(res)) {
        ui->statusbar->showMessage("Result is not a number.");
        valid = false;
    }

    this->currentOperation = Command::SetCalculatorValue;

    return QPair<double, bool>(res, valid);
}

void MainWindow::on_changeSign_clicked()
{
    ui->display->negate();
}

void MainWindow::on_delCurrent_clicked()
{
    ui->display->clearCurrent();
}

void MainWindow::on_delChar_clicked()
{
    ui->display->backspace();
}

void MainWindow::on_delAll_clicked()
{
    // Clear errors
    ui->statusbar->clearMessage();

    ui->display->clearAll();
    this->currentOperation = Command::SetCalculatorValue;
}

void MainWindow::on_undo_clicked()
{
    try {
        double val = backend.historyBackward();
        ui->display->setCurrentNum(val);
    } catch (const std::runtime_error &err) {
        ui->statusbar->showMessage(err.what());
    }
}

void MainWindow::on_redo_clicked()
{
    try {
        double val = backend.historyForward();
        ui->display->setCurrentNum(val);
    } catch (const std::runtime_error &err) {
        ui->statusbar->showMessage(err.what());
    }
}

void MainWindow::on_helpButton_clicked()
{
    this->setMaximumWidth(this->maximumWidth()+WIZARD_WIDTH);
    QSize newSize(maximumWidth(), height());
    this->resize(newSize);

    this->setEnabledAll(false);
    ui->helpButton->setEnabled(false);

    ui->helpWizard->setEnabled(true);
    ui->helpWizard->setHidden(false);

}
