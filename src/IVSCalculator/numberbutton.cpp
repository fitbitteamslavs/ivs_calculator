#include "numberbutton.h"

NumberButton::NumberButton(QWidget *parent) : SquareButton(parent)
{
    this->fontSize = 40;
}

void NumberButton::emitWithData()
{
    QVariant mesg(this->text());
    emit this->clickedWithData(mesg);
}
