#include "displaysection.h"
#include "ui_displaysection.h"

DisplaySection::DisplaySection(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::DisplaySection)
{
    ui->setupUi(this);
}

DisplaySection::~DisplaySection()
{
    delete ui;
}

void DisplaySection::setPrevNum(double num)
{
    if(clearOnNextInput) clearAll();
    if(!qIsInf(num))
        setPrevNum(QString::number(num));
    else {
        setPrevNum("∞");
    }
}

void DisplaySection::setOperation(QString operationRepr, QPair<double, bool> newValue, bool keepPrevVal)
{
    if(newValue.second) {
        if(clearOnNextInput) clearAll();
        if(keepPrevVal){
            setPrevNum(newValue.first);
            this->setCurrentNum("");
        } else {
            this->setCurrentNum(newValue.first);
            setPrevNum("");
        }
        ui->operationLabel->setText(operationRepr);
    }
}

void DisplaySection::setCurrentNum(double num)
{
    if(clearOnNextInput) clearAll();
    if(!qIsInf(num))
        setCurrentNum(QString::number(num));
    else {
        setCurrentNum("∞");
    }
}

void DisplaySection::printMessage(QString msg)
{
    if(clearOnNextInput) clearAll();
    qDebug() << "Printing message";
    ui->currentNum->setStyleSheet("QLineEdit { border: 1px solid grey; color: red; }");
    setCurrentNum(msg);
    this->clearOnNextInput = true;
}

void DisplaySection::addDigitToNumber(QChar c)
{
    if(clearOnNextInput) clearAll();
    setCurrentNum(getCurrentNumAsString().append(c));
}

void DisplaySection::negate()
{
    QString num = getCurrentNumAsString();
    if(num.length() > 0){
        if(num.at(0)=='-'){
            num.replace(0,1,"");
        } else {
            num.prepend('-');
        }
    } else {
        num.append('-');
    }
    setCurrentNum(num);
}

void DisplaySection::backspace()
{
    this->ui->currentNum->backspace();
}

double DisplaySection::getCurrentNum()
{
    return getCurrentNumAsString().toDouble();
}

QString DisplaySection::getCurrentNumAsString()
{
    if(clearOnNextInput) clearAll();
    return ui->currentNum->text();
}

void DisplaySection::clearAll()
{
    clearOnNextInput = false;
    ui->currentNum->setStyleSheet("QLineEdit { border: 1px solid grey; color: black; }");

    ui->operationLabel->setText("");
    ui->prevNum->setText("");
    ui->currentNum->setText("");
}

void DisplaySection::clearCurrent()
{
    setCurrentNum("");
}

void DisplaySection::setPrevNum(QString str)
{
    if(clearOnNextInput) clearAll();
    ui->prevNum->setText(str);
}

void DisplaySection::setCurrentNum(QString str)
{
    if(clearOnNextInput) clearAll();
    ui->currentNum->setText(str);
}
