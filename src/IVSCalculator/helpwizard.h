#ifndef HELPWIZARD_H
#define HELPWIZARD_H

#include <QWizard>
#include <QWizardPage>
#include <QLayout>
#include <QLabel>
#include <QDebug>

#include "libcalc.h"

/**
 * @file
 */

namespace Ui {
class HelpWizard;
}

/**
 * @ingroup GUI
 * @brief The HelpWizard class is widget for help/tutorial in the GUI enabled by '?' button
 */
class HelpWizard : public QWizard
{
    Q_OBJECT

public:
    /**
     * @brief Default widget constructor
     * @param parent Is the parent widget
     * @details Here the window type is set to Qt::Widget (from Qt::Dialog) and to Qt::NonModal
     * It is also disabled by default and the signal inherited from QWizard::currentIdChanged is
     * connected to private slot doActions()
     */
    explicit HelpWizard(QWidget *parent = nullptr);
    ~HelpWizard();

    /**
     * @brief The Pages enum Specifies different pages in the help
     */
    enum Pages {NoPrevious=-1, Intro, Sections, Add1, Add2, Add3, Add4, Add5, Add6, Pow1, Pow2, History, Fin};

public slots:

signals:
    /**
     * @brief Signal send when areas should be highlighted
     * @param high true if highlight, to disable false
     */
    void highlightAreas(bool high);
    /**
     * @brief Signal emmited when the Add button should be enabled/disabled
     * @param enable true if enable, false if disable
     */
    void enableAdd(bool enable);
    /**
     * @brief Signal emmited when the Second power button should be enabled/disabled
     * @param enable true if enable, false if disable
     */
    void enableSecPow(bool enable);
    /**
     * @brief Signal emmited when the numbers section should be enabled
     * @param enable true if enable, false if disable
     */
    void enableNums(bool enable);
    /**
     * @brief Signal emmited when the equals button should be enabled/disabled
     * @param enable true if enable, false if disable
     */
    void enableEquals(bool enable);
    /**
     * @brief Signal emmited when history Undo and Redo buttons should be enabled/disabled
     * @param enable true if enable, false if disable
     */
    void enableHistory(bool enable);

private slots:
    /**
     * @brief This slot resolves based on new page which signals should be emmited
     * @param actId Id of the Page one from  @ref Pages
     */
    void doActions(int actId);


private:
    Ui::HelpWizard *ui;
    /**
     * @brief prevId Stores previous page ID for doActions() to determine correct actions
     */
    int prevId = NoPrevious;
};

#endif // HELPWIZARD_H
