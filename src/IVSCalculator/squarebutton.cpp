#include "squarebutton.h"

SquareButton::SquareButton(QWidget *parent) : QToolButton(parent)
{
    this->setSizePolicy(QSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred));
    this->setMinimumSize(QSize(50,50));
    this->setMaximumSize(QSize(85,85));
    QObject::connect(this, &SquareButton::clicked, this, &SquareButton::emitWithData);
    QFont cur = this->font();
    cur.setPointSize(25);
    this->setFont(cur);
}

void SquareButton::setFontSize(int size)
{
    this->fontSize = size;
}

bool SquareButton::hasHeightForWidth() const
{
    return true;
}

int SquareButton::heightForWidth(int w) const
{
    return w;
}

int SquareButton::minimumHeightForWidth(int w)
{
    return w;
}

void SquareButton::setText(const QString &str)
{
    /* This piece of code was used from
     * https://www.qtcentre.org/threads/31094-QPushButton-with-a-custom-QLabel?p=145160#post145160
     */
    QTextDocument Text;
    Text.setDefaultStyleSheet("b {font-size: "+QString::number(this->fontSize)+"px}");
    Text.setHtml("<b>" + str + "<b>");

    QPixmap pixmap(Text.size().width(), Text.size().height());
    pixmap.fill( Qt::transparent );
    QPainter painter( &pixmap );
    Text.drawContents(&painter, pixmap.rect());

    QIcon ButtonIcon(pixmap);
    this->setIcon(ButtonIcon);
    this->setIconSize(pixmap.rect().size());

    QAbstractButton::setText(str);
}


void SquareButton::emitWithData()
{
}

