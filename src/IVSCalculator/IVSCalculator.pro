##### CUSTOM VARIABLES
PROJECT_ROOT=$$absolute_path("../../")
LIBCALC_BUILD_LOCATION=$${PROJECT_ROOT}/build/libcalc
##### END:CUSTOM VARIABLES

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = conputator

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    helpwizard.cpp \
    main.cpp \
    mainwindow.cpp \
    numberbutton.cpp \
    operationbutton.cpp \
    squarebutton.cpp \
    displaysection.cpp

HEADERS += \
    helpwizard.h \
    mainwindow.h \
    numberbutton.h \
    operationbutton.h \
    squarebutton.h \
    displaysection.h

FORMS += \
    helpwizard.ui \
    mainwindow.ui \
    displaysection.ui

LIBS += -L"$${LIBCALC_BUILD_LOCATION}" -lcalc
INCLUDEPATH += $$PROJECT_ROOT/src/libcalc/include

RC_ICONS = icon_ivscalc.png

CONFIG(debug, release|debug) {
	DESTDIR=$${PROJECT_ROOT}/build/debug 	# traditionally, build is located at top level
}

CONFIG(release, release|debug) {
	DESTDIR=$${PROJECT_ROOT}/build/release
}

OBJECTS_DIR = $$DESTDIR/.obj
MOC_DIR = $$DESTDIR/.moc
RCC_DIR = $$DESTDIR/.qrc
UI_DIR = $$DESTDIR/.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
