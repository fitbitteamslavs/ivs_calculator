#include <iostream> 
#include <vector>
#include "libcalc-math.h"

using namespace std;

double calculateStandardDeviation(vector<double> &dataset);

int main() {
    vector<double> input_numbers = vector<double>();
    
    double input_number;
    while (scanf("%lf", &input_number) != EOF) {
        input_numbers.push_back(input_number);
    }
    cout << calculateStandardDeviation(input_numbers);
    return 0;
}

double calculateStandardDeviation(vector<double> &dataset) {
    // calculate average, and sum of squares (as assignment says)
    double sum = 0.0;
    for (auto const &datapoint: dataset) {
        sum = libcalcMath::add(sum, datapoint);
    }
    double sum_of_squares = 0.0;

    vector<double> points_squared = vector<double>();
    points_squared.resize(dataset.size());
    double point_squared;

    for (int i = 0; i < dataset.size(); i++) {
        point_squared = libcalcMath::power(dataset[i], 2.0);
        points_squared[i] = point_squared;
    }

    for (auto const& datapoint_squared: points_squared) {
        sum_of_squares = libcalcMath::add(sum_of_squares, datapoint_squared); 
    }

    double average_factor = libcalcMath::power(sum, 2.0);
    average_factor = libcalcMath::div(average_factor, static_cast<double>(dataset.size()));

    double diff = libcalcMath::sub(sum_of_squares, average_factor); 

    double result = libcalcMath::sqrt(libcalcMath::div(diff, dataset.size() - 1), 1.0);

    return result;
}

