#include <iostream>
#include <fstream>
#include <vector>
#include "libcalc.h"
#include "libcalc-math.h"

double calculateStandardDeviation(Calculator *c, CommandFactory *f, vector<double> &data);

int main(int argc, char *argv[]) {
    using namespace std;
    Calculator *c = new Calculator(0.0);
    CommandFactory *f = new CommandFactory(c);
    vector<double> input_numbers = vector<double>();

    double current_num;
    while (scanf("%lf", &current_num) != EOF) {
        input_numbers.push_back(current_num); 
    }

    cout << calculateStandardDeviation(c, f, input_numbers);

	return 0;
}

double calculateStandardDeviation(Calculator *c, CommandFactory *f,vector<double> &data) {
    // SUM
    for (auto datapoint: data){
        Command cmd = f->createCommand(Command::CommandType::Add, datapoint);
        c->applyCommand(cmd);
    }
    Command cmd = f->createCommand(Command::CommandType::Div, (double) data.size());
    c->applyCommand(cmd); // Average
    cmd = f->createCommand(Command::CommandType::NthPower,  2.0);
    c->applyCommand(cmd);
    cmd = f->createCommand(Command::CommandType::Mul, (double) data.size());
    c->applyCommand(cmd); 

    double average_weighted = c->getCurrentState(); // N*x^2

    vector<double> data_squared = vector<double>();
    data_squared.resize(data.size());

    cmd = f->createCommand(Command::CommandType::SetCalculatorValue, 0.0);
    c->applyCommand(cmd);
    double data_point_squared;
    for (int i = 0; i < data.size(); i++) {
        cmd = f->createCommand(Command::CommandType::SetCalculatorValue, data[i]);
        c->applyCommand(cmd);
        cmd = f->createCommand(Command::CommandType::NthPower, 2.0);
        c->applyCommand(cmd);
        data_point_squared = c->getCurrentState();
        data_squared[i] = data_point_squared;
    }
   
    // Calculate sum
    cmd = f->createCommand(Command::CommandType::SetCalculatorValue, 0.0);
    c->applyCommand(cmd);
    for (auto datapoint_squared: data_squared) {
        cmd = f->createCommand(Command::CommandType::Add, datapoint_squared);
        c->applyCommand(cmd);
    }
    cmd = f->createCommand(Command::CommandType::Sub, average_weighted);
    c->applyCommand(cmd);
    cmd = f->createCommand(Command::CommandType::Div, data.size()-1);
    c->applyCommand(cmd);
    cmd = f->createCommand(Command::CommandType::Sqrt, 1);
    c->applyCommand(cmd);
    return c->getCurrentState();
}
