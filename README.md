# Conputator

This is a simple calculator app, we created for an IVS project at VUT FIT Brno.

## Rules
1. All builds should be done in `build/` directory, which is excluded in git
2. All sourcecodes are in `src/` and in custom directory (`library/`,`profile/` etc.) 


## Building & installation
### Prerequisites
When building `conputator` you will need following packages installed on your machine (for Ubuntu 18.04):
- build-essentials
- qt5-default

One of the dependencies of this project is [googletest](https://github.com/google/googletest). When building the repository
for the first time, the Makefile will ask you whether you wish googletest to be installed. Keep in mind, that
if you agree to that, you will have to have installed all of the dependencies of googletest, because the makefile
will download and build it from source. Otherwise you may modify a variable inside Makefile - `GTEST_HOME`, to
point where your googletest is. Note that this variable should point to the root of (compiled) googletest repository.

### Basic build steps
First enter the `src/` directory, and execute make:
```bash
make
```

If it is your first time runninig Makefile, following prompt may appear:
```
Creating gtest configuration file..
Enviromental variable $GTEST_HOME not set, however GTest is required to compile tests.
Would you like to download it? y/n:
```
In this case either type `n` and set the `GTEST_HOME` variable in `src/Makefile`, or type `y`, to
let the automatic download of googletest happen (make sure that you have installed all dependencies to
googletest beforehand).

After that project will build. The default location of build is `build` folder in the root of the directory.
Documentation will be placed in `docs` directory.

### Produced artifacts
- Library binary (shared object): `build/libcalc/libcalc.so.1`
- Graphical calculator: `build/release/conputator`
- Profiling executable: `build/profiling/profile_nocalc`
- Tests executable: `build/tests/libcalc_test`

You may execute all of the artifacts (except shared object) normally, however make sure that
the shared object is in the `LD_LIBRARY_PATH`.

### Rebuilding specific components
Specific targets in make let you rebuild specified artifacts:

|         target name         |     rebuilds     |  depends on   |
|-----------------------------|------------------|---------------|
|        build-library        |     library      |       x       |
|          build-ui           | (GUI) conputator | build-library |
|         build-tests         |      tests       | build-library |
| build-profile-no-calculator |  profiling code  | build-library |


### Installation
Tested on Ubuntu 64bit
#### How to install

1. Build the program:
```bash
$ cd ./src
$ make
```
Note that now you will be asked whether you want to install [googletest](https://github.com/google/googletest) **automatically**, for further informations read Basic build steps section.

2. Installation:
```bash
$ sudo make install
```
Note that this requires **sudo** to access `/usr/bin` and `/usr/lib/` directories since the default installation destination is there.

3. To install icon and .desktop file (optional):
```bash
$ sudo make install_shortcuts
```
Note that this requires **sudo** to coppy files `/usr/shared/` subdirectories.

4. To run this program:
```bash
$ conputator
```
Also you you should be able to find this program in menu and run it from there.
#### How to uninstall

1. To uninstall:
```bash
$ sudo make uninstall
```
2. To delete shortcuts
```bash
$ sudo make uninstall_shortcuts
```
3. To delete build (optional):
```bash
$ make clean
```

### Tests

There are also tests for the mathematical library of this project. These tests are built after the installation and can be run (from root of this project):

```bash
cd ./build/tests
LD_LIBRARY_PATH=../libcalc ./libcalc_test
```
Note that if you have the `conputator` propperly installed there is no need to set `LD_LIBRARY_PATH` before runing tests.

# Authors

* [**Michal Hečko**](https://gitlab.com/__codeboy) - xhecko02
* [**Dávid Lacko**](https://gitlab.com/deiwo)  - *xlacko09*
* [**Peter Močáry**](https://gitlab.com/pET3RS) - *xmocar00*

# License

This program is licensed under GPL v3.

# Build problems
## Googletest is failing to link
When the build process is failing on linking googletest, it is most likely that `GTEST_HOME` has been
incorrectly specified. This does not usually happen when you let the Makefile download the library.

If this situation occurres, two solutions are possible. 
- You may manually edit the `gtest_config.mk` which is located at the root of the repository to 
contain valid path to googletest.
- You can delete the `gtest_config.mk` and execute `make` again. This however executes the internal
test setup wizard again, which may present the interactive prompt. Consider setting `GTEST_HOME` when
building the project.
